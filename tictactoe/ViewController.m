//
//  ViewController.m
//  tictactoe
//
//  Created by Patrick Madden on 3/15/16.
//  Copyright © 2016 CS441 SUNY Binghamton. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize gameTiles;

int gameBoard[9];

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	[self reset:nil];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(IBAction)playerMove:(id)sender
{
	UIButton *b = (UIButton *)sender;
	NSLog(@"Touch %d", (int)[sender tag]);
	int n = (int)[sender tag];
	
	if (gameBoard[n] != 0)
	{
		NSLog(@"Already touched");
	}
	else
	{
		[b setTitle:@"X" forState:UIControlStateNormal];
		gameBoard[n] = 1;
		
		[self evaluateBoard:gameBoard player:1];
		int nextMove = [self suggestMove:gameBoard player:2];
		if (nextMove >= 0)
		{
			for (UIButton *b2 in gameTiles)
			{
				
				if ([b2 tag] == nextMove)
				{
					[b2 setTitle:@"O" forState:UIControlStateNormal];
					gameBoard[nextMove] = 2;
				}
			}
		}
	}
}

-(int)suggestMove:(int *)board player:(int)player
{
	for (int i = 0; i < 9; ++i)
	{
		if (board[i] == 0)
			return i;
	}
	return -1;
}

-(IBAction)reset:(id)sender
{
	for (UIButton *b in gameTiles)
	{
		[b setTitle:@"--" forState:UIControlStateNormal];
		gameBoard[(int)[b tag]] = 0;
	}
}

static int win1[] = {0, 1, 2};  // Horizontal wins
static int win2[] = {3, 4, 5};
static int win3[] = {6, 7, 8};
static int win4[] = {0, 3, 6};  // Vertical wins
static int win5[] = {1, 4, 7};
static int win6[] = {2, 5, 8};
static int win7[] = {0, 4, 8};  // Upper left to lower right
static int win8[] = {2, 4, 6};  // Upper right to lower left
static int *wins[] = {win1, win2, win3, win4, win5, win6, win7, win8};


-(int)evaluateBoard:(int *)board player:(int)player
{
	int i, j;

	
	for (i = 0; i < 8; ++i)
	{
		int didWin = 1;
		int *positions = wins[i];

		for (j = 0; j < 3; ++j)
		{
			if (board[positions[j]] != player)
				didWin = 0;
		}
		if (didWin)
		{
			NSLog(@"Win for player %d game type %d", player, i);
			return 1;
		}
	}
	
	return 0;
}
@end
