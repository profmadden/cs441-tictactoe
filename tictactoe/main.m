//
//  main.m
//  tictactoe
//
//  Created by Patrick Madden on 3/15/16.
//  Copyright © 2016 CS441 SUNY Binghamton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
