//
//  AppDelegate.h
//  tictactoe
//
//  Created by Patrick Madden on 3/15/16.
//  Copyright © 2016 CS441 SUNY Binghamton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

